using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class ToggleManager : MonoBehaviour
{
    public List<Toggle> toggles = new List<Toggle>(); // Lista de Toggles
    public Button myButton; // Referencia al bot�n
    public int maxItems = 3;
    public int fillBefore = 0;
    public int fillAfter = 1;

    void Start()
    {
        myButton.interactable = false;
        // Inicializar cada Toggle
        foreach (var toggle in toggles)
        {
            toggle.onValueChanged.AddListener(delegate { ToggleChanged(); });
        }

        // Inicializar el bot�n
        UpdateButtonInteractivity();
    }

    // Se llama cada vez que se cambia un Toggle
    void ToggleChanged()
    {
        int activeCount = 0; // Contador para Toggles activos

        // Contar cu�ntos Toggles est�n activos
        foreach (var toggle in toggles)
        {
            if (toggle.isOn)
            {
                activeCount++;
                if (activeCount > maxItems)
                {
                    toggle.isOn = false; // Desactivar el �ltimo Toggle si hay m�s de 3
                    return;
                }
            }
        }

        UpdateButtonInteractivity();
    }

    // Actualizar la interactividad del bot�n
    void UpdateButtonInteractivity()
    {
        int activeCount = 0;
        foreach (var toggle in toggles)
        {
            if (toggle.isOn)
                activeCount++;
        }

        myButton.interactable = activeCount >= 1 && activeCount <= maxItems;

        UpdateFillButton(myButton.interactable);
    }

    void UpdateFillButton(bool interactable)
    {
        if(interactable)
        {
            myButton.transform.GetChild(0).GetComponent<Image>().fillAmount = fillAfter * 1f / 7f;
        }
        else
        {
            myButton.transform.GetChild(0).GetComponent<Image>().fillAmount = fillBefore * 1f / 7f;
        }
    }
 
}
