﻿namespace UnityEngine.UI.Extensions.Examples
{
    public class ScrollMonths : MonoBehaviour
    {
        public RectTransform languageScrollingPanel;
        public ScrollRect languageScrollRect;
        public GameObject languageButtonPrefab;
        private GameObject[] languageButtons;
        public RectTransform languageCenter;
        UIVerticalScroller languageVerticalScroller;
        public Text dateText;

        private void Initializelanguage()
        {
            int[] language = new int[12];

            languageButtons = new GameObject[language.Length];
            for (int i = 0; i < language.Length; i++)
            {
                string month = "";
                language[i] = i;

                GameObject clone = Instantiate(languageButtonPrefab, languageScrollingPanel);
                clone.transform.localScale = new Vector3(1, 1, 1);

                switch (i)
                {
                    case 11:
                        month = "January";
                        break;
                    case 10:
                        month = "February";
                        break;
                    case 9:
                        month = "March";
                        break;
                    case 8:
                        month = "April";
                        break;
                    case 7:
                        month = "May";
                        break;
                    case 6:
                        month = "June";
                        break;
                    case 5:
                        month = "July";
                        break;
                    case 4:
                        month = "August";
                        break;
                    case 3:
                        month = "September";
                        break;
                    case 2:
                        month = "October";
                        break;
                    case 1:
                        month = "November";
                        break;
                    case 0:
                        month = "December";
                        break;
                }

                clone.GetComponentInChildren<Text>().text = month;
                clone.name = "Month_" + language[i];
                clone.AddComponent<CanvasGroup>();
                languageButtons[i] = clone;
            }
        }

        public void Awake()
        {
            Initializelanguage();
            languageVerticalScroller = new UIVerticalScroller(languageCenter, languageCenter, languageScrollRect, languageButtons);
            languageVerticalScroller.Start();
        }

        void Update()
        {
            languageVerticalScroller.Update();
            string languagetring = languageVerticalScroller.Result;

            dateText.text = languagetring;
        }
    }
}