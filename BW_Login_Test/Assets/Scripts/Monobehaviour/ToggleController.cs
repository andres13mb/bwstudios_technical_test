using UnityEngine;
using UnityEngine.UI;

public class ToggleController : MonoBehaviour
{
    private Toggle mainToggle;
    private GameObject mark;

    void Start()
    {
        mainToggle = GetComponent<Toggle>();
        mark = mainToggle.graphic.rectTransform.GetChild(0).gameObject;

        if (mainToggle != null)
        {
            mainToggle.onValueChanged.AddListener(delegate {
                ToggleValueChanged(mainToggle);
            });
        }

        mark.SetActive(false);
    }

    void ToggleValueChanged(Toggle change)
    {
        mark.SetActive(change.isOn);
    }
}
