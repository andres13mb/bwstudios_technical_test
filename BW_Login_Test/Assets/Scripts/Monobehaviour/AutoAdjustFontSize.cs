using UnityEngine;
using TMPro;

public class AutoAdjustFontSize : AutoAdjust
{
    private TMP_Text textComponent;

    [SerializeField] private float minFontSize;
    [SerializeField] private float maxFontSize;
    [SerializeField] private float anchoredPositionOffsetY = 0;

    protected override void Start()
    {
        textComponent = GetComponent<TMP_Text>();
        base.Start();
    }

    protected override void Adjust()
    {
        float t = (currentAspectRatio - aspectRatioBaseTablet) / (aspectRatioBaseMobile - aspectRatioBaseTablet);
        float interpolatedFontSize = Mathf.Lerp(minFontSize, maxFontSize, t);
        textComponent.fontSize = interpolatedFontSize;
        float offsetMultiplierY = Mathf.Lerp(1, anchoredPositionOffsetY, t);

        elementRectTransform.anchoredPosition = new Vector2(elementRectTransform.anchoredPosition.x,
            elementRectTransform.anchoredPosition.y + offsetMultiplierY);
    }
}
