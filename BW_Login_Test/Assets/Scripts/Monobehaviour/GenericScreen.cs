using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GenericScreen : MonoBehaviour
{
    public Button nextButton;
    public Button backButton;
    public Button openPopupButton;

    public Button openInitialButton;
    public Button openProfileButton;
    public GameObject popupPrefab;

    private UIManager uiManager;
    private GameObject popupInstance;
    public bool isAutomatic = false;
    private readonly float delay = 2f;
    private Coroutine automaticChangeCoroutine;

    void Start()
    {
        if (nextButton != null)
            nextButton.onClick.AddListener(() => {
                StopAutomaticChange();
                uiManager.GoToNextScreen();
            });

        if (backButton != null)
            backButton.onClick.AddListener(() => {
                StopAutomaticChange();
                uiManager.GoToPreviousScreen();
            });

        if (openPopupButton != null)
            openPopupButton.onClick.AddListener(OpenPopup);

        if (openInitialButton != null)
            openInitialButton.onClick.AddListener(OpenInitial);
    }

    void OnEnable()
    {
        if (isAutomatic)
        {
            automaticChangeCoroutine = StartCoroutine(AutomaticScreenChange());
        }
    }

    public void Initialize(UIManager manager)
    {
        uiManager = manager;
    }

    private IEnumerator AutomaticScreenChange()
    {
        yield return new WaitForSeconds(delay);
        uiManager.GoToNextScreen();
    }

    void OnDisable()
    {
        StopAutomaticChange();
    }

    private void StopAutomaticChange()
    {
        if (automaticChangeCoroutine != null)
        {
            StopCoroutine(automaticChangeCoroutine);
            automaticChangeCoroutine = null;
        }
    }

    private void OpenPopup()
    {
        if (popupInstance == null)
        {
            popupInstance = Instantiate(popupPrefab, transform.parent);
            var popupManager = popupInstance.GetComponent<PopupManager>();
            if (popupManager != null)
            {
                popupManager.OpenPopup();
                popupManager.Initialize(uiManager);
            }
        }
        else
        {
            popupInstance.SetActive(true);
            popupInstance.GetComponent<PopupManager>().OpenPopup();
        }
    }
    private void OpenInitial()
    {
        uiManager.OpenInitialScreen();
    }

    private void OpenProfile()
    {
        uiManager.OpenProfileScreen();
    }
}
