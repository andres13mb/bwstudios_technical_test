﻿namespace UnityEngine.UI.Extensions.Examples
{
    public class ScrollLanguages : MonoBehaviour
    {
        public RectTransform languagesScrollingPanel;
        public ScrollRect languagesScrollRect;
        public GameObject languagesButtonPrefab;
        private GameObject[] languagesButtons;
        public RectTransform languageCenter;
        UIVerticalScroller languagesVerticalScroller;
        public Text dateText;

        private void Initializelanguages()
        {
            int[] languages = new int[3];

            languagesButtons = new GameObject[languages.Length];
            for (int i = 0; i < languages.Length; i++)
            {
                string language = "";
                languages[i] = i;

                GameObject clone = Instantiate(languagesButtonPrefab, languagesScrollingPanel);
                clone.transform.localScale = new Vector3(1, 1, 1);

                switch (i)
                {
                    case 0:
                        language = "Portuguese";
                        break;
                    case 1:
                        language = "English";
                        break;
                    case 2:
                        language = "Spanish";
                        break;                 
                }

                clone.GetComponentInChildren<Text>().text = language;
                clone.name = "language_" + languages[i];
                clone.AddComponent<CanvasGroup>();
                languagesButtons[i] = clone;
            }
        }

        public void Awake()
        {
            Initializelanguages();
            languagesVerticalScroller = new UIVerticalScroller(languageCenter, languageCenter, languagesScrollRect, languagesButtons);
            languagesVerticalScroller.Start();
        }

        void Update()
        {
            languagesVerticalScroller.Update();
            string languageString = languagesVerticalScroller.Result;

            dateText.text = languageString;
        }
    }
}