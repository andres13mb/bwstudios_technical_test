using UnityEngine;

public abstract class AutoAdjust : MonoBehaviour
{
    protected float currentAspectRatio;
    protected float aspectRatioBaseTablet = 2048f / 1536f;
    protected float aspectRatioBaseMobile = 1554f / 718f;
    protected RectTransform elementRectTransform;

    protected virtual void Start()
    {
        elementRectTransform = GetComponent<RectTransform>();
        currentAspectRatio = (float)Screen.height / Screen.width;
        Adjust();
    }

    protected abstract void Adjust();
}

