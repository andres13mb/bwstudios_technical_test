using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PopupManager : MonoBehaviour
{
    [SerializeField] private Image backgroundImage;
    [SerializeField] private RectTransform popupPanel;
    [SerializeField] private float fadeInDuration = 0.5f;
    [SerializeField] private float panelMoveDuration = 0.5f;

    public Button openLoginEmailButton;
    public Button openLoginGoogleButton;
    private UIManager uiManager;

    void Start()
    {
        // Iniciar con el fondo transparente y el panel fuera de la pantalla
        backgroundImage.color = new Color(0, 0, 0, 0);
        popupPanel.anchoredPosition = new Vector2(0, -2048 / 2);

        if (openLoginEmailButton != null)
            openLoginEmailButton.onClick.AddListener(LoginWithEmail);

        if (openLoginGoogleButton != null)
            openLoginGoogleButton.onClick.AddListener(LoginWithGoogle);
    }

    public void Initialize(UIManager manager)
    {
        uiManager = manager;
    }

    public void OpenPopup()
    {
        Sequence sequence = DOTween.Sequence();

        // Fade in del fondo con fadeInDuration
        sequence.Append(backgroundImage.DOColor(new Color(0, 0, 0, 0.75f), fadeInDuration));

        // Desplazamiento del panel con panelMoveDuration
        sequence.Append(popupPanel.DOAnchorPos(Vector2.zero, panelMoveDuration));

        sequence.Play();
    }

    public void ClosePopup()
    {
        Sequence sequence = DOTween.Sequence();

        sequence.Append(popupPanel.DOAnchorPos(new Vector2(0, -2048 / 2), panelMoveDuration));
        sequence.Append(backgroundImage.DOColor(new Color(0, 0, 0, 0), fadeInDuration)).OnComplete(() => gameObject.SetActive(false));

        sequence.Play();
    }

    private void LoginWithEmail()
    {
        ClosePopup();
        uiManager.OpenLoginEmailScreen();        
    }

    private void LoginWithGoogle()
    {
        ClosePopup();
        FirebaseGoogleLogin.Get().GoogleSignInClick();
    }    
}
