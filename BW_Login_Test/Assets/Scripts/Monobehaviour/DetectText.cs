using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DetectText : MonoBehaviour
{
    public TMP_InputField nameInputfield;
    public Button myButton;
    //private bool isFirstTime = true;

    //private void Start()
    //{
    //    if(isFirstTime)
    //    {
    //        isFirstTime = false;
    //    }
    //}

    //private void OnEnable()
    //{
    //    if(!isFirstTime)
    //    {
    //        ClearInputfield();
    //        StartCoroutine(ClearInputfield());
    //    }
    //}

    //IEnumerator ClearInputfield()
    //{
    //    yield return new WaitForSeconds(0.17f);
    //    nameInputfield.text = "";
    //}

    public void VerfifyText()
    {
        if(nameInputfield.text != "")
        {
            myButton.interactable = true;
            myButton.transform.GetChild(0).GetComponent<Image>().fillAmount = 6 * 1f / 7f;
        }
        else
        {
            myButton.interactable = false;
            myButton.transform.GetChild(0).GetComponent<Image>().fillAmount = 5 * 1f / 7f;
        }
    }
}
