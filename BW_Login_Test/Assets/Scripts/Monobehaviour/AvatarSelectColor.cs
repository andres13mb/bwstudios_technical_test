using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AvatarSelectColor : MonoBehaviour
{
    [SerializeField] private Image avatarImage;

    private void Start()
    {
        GetComponent<Button>().onClick.AddListener(SelectAvatarColor);

    }

    public void SelectAvatarColor()
    {
        avatarImage.color = GetComponent<Image>().color;
    }   
}
