using UnityEngine;

public class AutoAdjustElement : AutoAdjust
{
    private float minSizeMultiplierX = 1f;
    private float minSizeMultiplierY = 1f;
    [SerializeField] private float maxSizeMultiplierX = 1.4f;
    [SerializeField] private float maxSizeMultiplierY = 1.4f;
    [SerializeField] private float anchoredPositionOffsetY = 0;

    protected override void Start()
    {
        base.Start();
    }

    protected override void Adjust()
    {
        float t = (currentAspectRatio - aspectRatioBaseTablet) / (aspectRatioBaseMobile - aspectRatioBaseTablet);
        float sizeMultiplierX = Mathf.Lerp(minSizeMultiplierX, maxSizeMultiplierX, t);
        float sizeMultiplierY = Mathf.Lerp(minSizeMultiplierY, maxSizeMultiplierY, t);
        float offsetMultiplierY = Mathf.Lerp(1, anchoredPositionOffsetY, t);

        elementRectTransform.sizeDelta = new Vector2(elementRectTransform.sizeDelta.x * sizeMultiplierX,
            elementRectTransform.sizeDelta.y * sizeMultiplierY);

        elementRectTransform.anchoredPosition = new Vector2(elementRectTransform.anchoredPosition.x,
            elementRectTransform.anchoredPosition.y + offsetMultiplierY);
    }
}
