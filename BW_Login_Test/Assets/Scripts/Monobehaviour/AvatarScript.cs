using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class AvatarScript : MonoBehaviour
{
    public TMP_InputField nameInputfield;
    public Button myButton;
    private bool isFirstTime = true;

    private void Start()
    {
        if (isFirstTime)
        {
            isFirstTime = false;
        }
    }

    private void OnEnable()
    {
        if (!isFirstTime)
        {
            ClearInputfield();
            StartCoroutine(ClearInputfield());
        }
    }

    IEnumerator ClearInputfield()
    {
        yield return new WaitForSeconds(0.17f);
        nameInputfield.text = "";
    }

    public void SaveName()
    {
        if (nameInputfield.text != "")
        {
            PlayerPrefs.SetString("name", nameInputfield.text);
        }
    }
}
