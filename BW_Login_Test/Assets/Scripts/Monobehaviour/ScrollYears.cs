﻿namespace UnityEngine.UI.Extensions.Examples
{
    public class ScrollYears : MonoBehaviour
    {
        public RectTransform yearsScrollingPanel;
        public ScrollRect yearsScrollRect;
        public GameObject yearsButtonPrefab;
        private GameObject[] yearsButtons;
        public RectTransform yearsCenter;
        UIVerticalScroller yearsVerticalScroller;
        public Text dateText;

        private void InitializeYears()
        {
            int currentYear = int.Parse(System.DateTime.Now.ToString("yyyy"));
            int[] arrayYears = new int[currentYear + 1 - 2013];
            yearsButtons = new GameObject[arrayYears.Length];

            for (int i = 0; i < arrayYears.Length; i++)
            {
                arrayYears[i] = 2013 + i;

                GameObject clone = Instantiate(yearsButtonPrefab, yearsScrollingPanel);
                clone.transform.localScale = new Vector3(1, 1, 1);
                clone.GetComponentInChildren<Text>().text = "" + arrayYears[i];
                clone.name = "Year_" + arrayYears[i];
                clone.AddComponent<CanvasGroup>();
                yearsButtons[i] = clone;
            }
        }

        public void Awake()
        {
            InitializeYears();
            yearsVerticalScroller = new UIVerticalScroller(yearsCenter, yearsCenter, yearsScrollRect, yearsButtons);
            yearsVerticalScroller.Start();
        }

        void Update()
        {     
            yearsVerticalScroller.Update();
            string yearsString = yearsVerticalScroller.Result;
            dateText.text =  yearsString;
        }      
    }
}