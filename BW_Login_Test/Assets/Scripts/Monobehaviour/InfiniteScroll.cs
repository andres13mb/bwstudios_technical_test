﻿using UnityEngine;
using UnityEngine.EventSystems;

public class InfiniteScroll : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
{
    public RectTransform contentPanel;
    private float[] itemPositions;
    private int itemCount;
    private float thresholdDistance; // Distancia para mover el elemento al otro extremo
    private float itemWidth;

    void Start()
    {
        itemCount = contentPanel.childCount;
        itemPositions = new float[itemCount];

        if (itemCount > 0)
        {
            itemWidth = contentPanel.GetChild(0).GetComponent<RectTransform>().rect.width;
            thresholdDistance = itemWidth * itemCount / 2;
        }

        UpdateItemPositions();
    }

    public void OnDrag(PointerEventData eventData)
    {
        UpdateItemPositions();

        for (int i = 0; i < itemCount; i++)
        {
            if (itemPositions[i] > thresholdDistance)
            {
                // Mueve el elemento al final
                contentPanel.GetChild(i).SetAsFirstSibling();
                UpdateItemPositions(); // Actualiza las posiciones después de reordenar
            }
            else if (itemPositions[i] < -thresholdDistance)
            {
                // Mueve el elemento al inicio
                contentPanel.GetChild(i).SetAsLastSibling();
                UpdateItemPositions(); // Actualiza las posiciones después de reordenar
            }
        }
    }

    private void UpdateItemPositions()
    {
        for (int i = 0; i < itemCount; i++)
        {
            itemPositions[i] = contentPanel.GetChild(i).GetComponent<RectTransform>().anchoredPosition.x;
        }
    }

    public void OnBeginDrag(PointerEventData eventData) { }

    public void OnEndDrag(PointerEventData eventData) { }
}
