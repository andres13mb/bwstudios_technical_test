using UnityEngine;
using Zenject;

public class MainInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        // Inyecci�n de dependencias
        Container.Bind<IAuthenticator>().To<AuthenticationService>().AsSingle();
        Container.Bind<UserService>().AsSingle();
        //Container.Bind<IScreenManager>().FromComponentInHierarchy().AsSingle();
        Container.Bind<IUIManager>().FromComponentInHierarchy().AsSingle();
    }
}