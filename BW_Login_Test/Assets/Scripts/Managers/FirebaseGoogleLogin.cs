using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using Firebase;
using Firebase.Extensions;
using Firebase.Auth;
using UnityEngine.UI;
using Google;
using System.Net.Http;
using TMPro;
using UnityEngine.SceneManagement;
using Zenject;
using UnityEngine.Networking;

public class FirebaseGoogleLogin : MonoBehaviour
{
    public static FirebaseGoogleLogin Instance;

    [Inject] private IUIManager uIManager;

    // ID de la aplicaci�n de Google
    public string GoogleWebAPI = "316581956196-fdabl2hcp4qfohunaqn7s020k35tr2rb.apps.googleusercontent.com";

    private GoogleSignInConfiguration configuration;

    Firebase.DependencyStatus dependencyStatus = Firebase.DependencyStatus.UnavailableOther;
    Firebase.Auth.FirebaseAuth auth;
    Firebase.Auth.FirebaseUser user;

    public string imageUrl;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        // Configuraci�n de inicio de sesi�n de Google
        configuration = new GoogleSignInConfiguration
        {
            WebClientId = GoogleWebAPI,
            RequestIdToken = true
        };
    }

    private void Start()
    {
        // Inicializaci�n de Firebase
        InitFirebase();
    }

    // Inicializaci�n de Firebase
    private void InitFirebase()
    {
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
    }

    // M�todo para el inicio de sesi�n con Google
    public void GoogleSignInClick()
    {
        GoogleSignIn.Configuration = configuration;
        GoogleSignIn.Configuration.UseGameSignIn = false;
        GoogleSignIn.Configuration.RequestIdToken = true;
        GoogleSignIn.Configuration.RequestEmail = true;
        GoogleSignIn.DefaultInstance.SignIn().ContinueWith(OnGoogleAuthenticatedFinished);
    }

    // M�todo que se llama cuando se ha completado el inicio de sesi�n con Google
    private void OnGoogleAuthenticatedFinished(Task<GoogleSignInUser> task)
    {
        if (task.IsFaulted)
        {
            // Manejo de error al iniciar sesi�n con Google
            Debug.LogError("Error al iniciar sesi�n");
        }
        else if (task.IsCanceled)
        {
            // Manejo de cancelaci�n del inicio de sesi�n con Google
            Debug.LogError("Inicio de sesi�n cancelado");
        }
        else
        {
            Firebase.Auth.Credential credential = Firebase.Auth.GoogleAuthProvider.GetCredential(task.Result.IdToken, null);

            // Iniciar sesi�n con las credenciales de Google en Firebase
            auth.SignInWithCredentialAsync(credential).ContinueWithOnMainThread(task =>

            {
                if (task.IsCanceled)
                {
                    // Manejo de cancelaci�n del inicio de sesi�n en Firebase
                    Debug.LogError("SignInWithCredentialAsync fue cancelado. ");
                    return;
                }
                if (task.IsFaulted)
                {
                    // Manejo de error al iniciar sesi�n en Firebase
                    Debug.LogError("SignInWithCredentialAsync encontr� un error. " + task.Exception);
                    return;
                }

                // Obtener usuario actual de Firebase
                user = auth.CurrentUser;

                // Mostrar pantalla de perfil
                uIManager.OpenProfileScreen();
                StartCoroutine(RefreshUserData(user.DisplayName, user.Email));

                // Cargar imagen del perfil
                StartCoroutine(LoadImage(CheckImageUrl(user.PhotoUrl.ToString())));
            });
        }
    }

    IEnumerator RefreshUserData(string name, string email)
    {
        yield return new WaitForSeconds(1f);

        if (uIManager.GetCurrentScreen().GetComponent<ProfileController>())
        {
            uIManager.GetCurrentScreen().GetComponent<ProfileController>().nameText.text = name;
            uIManager.GetCurrentScreen().GetComponent<ProfileController>().emailText.text = email;
        }
    }

    // Verificar la URL de la imagen del perfil
    private string CheckImageUrl(string url)
    {
        if (!string.IsNullOrEmpty(url))
        {
            return url;
        }

        return imageUrl;
    }

    // Cargar imagen del perfil
    IEnumerator LoadImage(string imageUrl)
    {
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(imageUrl);
        yield return www.SendWebRequest();

        if (www.result == UnityWebRequest.Result.ConnectionError || www.result == UnityWebRequest.Result.ProtocolError)
        {
            Debug.LogError(www.error);
        }
        else
        {
            Texture2D texture = DownloadHandlerTexture.GetContent(www);

            if (uIManager.GetCurrentScreen().GetComponent<ProfileController>())
            {
                uIManager.GetCurrentScreen().GetComponent<ProfileController>().imageProfile.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0));
            }
        }
    }

    // Cierra ses��n
    public void LogOut()
    {
        auth.SignOut();

        if (uIManager.GetCurrentScreen().GetComponent<ProfileController>())
        {
            uIManager.GetCurrentScreen().GetComponent<ProfileController>().DeleteAllData();
            uIManager.OpenInitialScreen();

            Debug.Log("Log out via Google");
        }

    }

    public static FirebaseGoogleLogin Get()
    {
        return Instance;
    }
}