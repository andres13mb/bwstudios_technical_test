using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Zenject;

public class UIManager : MonoBehaviour, IUIManager
{
    [System.Serializable]
    public class ScreenInfo
    {
        public string screenName;
        public GameObject prefab;
        public RectTransform instance;
    }

    public List<ScreenInfo> screenInfos = new List<ScreenInfo>();
    private RectTransform currentScreen;
    private int currentIndex = -1;
    public float transitionDuration = 0.5f;

    private IAuthenticator _authenticator;

    [Inject]
    public void Construct(IAuthenticator authenticator)
    {
        _authenticator = authenticator;
    }

    private void Start()
    {
        if(PlayerPrefs.HasKey("email"))
        {
            Debug.Log("Usuario existe");
            OpenLoginEmailScreen();
        }
        else
        {
            ShowScreen("Inicio", true);
        }
    }
    public void ShowScreen(string screenName, bool forward)
    {
        int screenIndex = screenInfos.FindIndex(s => s.screenName == screenName);
        if (screenIndex == -1)
        {
            Debug.LogError("Pantalla no encontrada: " + screenName);
            return;
        }

        currentIndex = screenIndex;

        ScreenInfo screenInfo = screenInfos[screenIndex];
        if (screenInfo.instance == null)
        {
            GameObject newScreenObj = Instantiate(screenInfo.prefab, transform);
            screenInfo.instance = newScreenObj.GetComponent<RectTransform>();

            var screenScript = newScreenObj.GetComponent<GenericScreen>();
            if (screenScript != null)
            {
                screenScript.Initialize(this);
            }

            var registerScript = newScreenObj.GetComponent<RegisterController>();
            if (registerScript != null)
            {
                registerScript.Initialize(_authenticator);
            }

            var loginScript = newScreenObj.GetComponent<LoginController>();
            if (loginScript != null)
            {
                loginScript.Initialize(_authenticator);
            }

            var profileScript = newScreenObj.GetComponent<ProfileController>();
            if (profileScript != null)
            {
                profileScript.Initialize(_authenticator);
            }
        }
      
        RectTransform screenToShow = screenInfo.instance;
        screenToShow.gameObject.SetActive(true);

        float canvasWidth = screenToShow.parent.GetComponent<RectTransform>().rect.width;
        Vector2 offScreenPosition = forward ? new Vector2(canvasWidth, 0) : new Vector2(-canvasWidth, 0);
        screenToShow.anchoredPosition = offScreenPosition;
        screenToShow.DOAnchorPos(Vector2.zero, transitionDuration);

        RectTransform screenToHide = currentScreen;
        if (screenToHide != null)
        {
            Vector2 exitPosition = forward ? new Vector2(-canvasWidth, 0) : new Vector2(canvasWidth, 0);
            screenToHide.DOAnchorPos(exitPosition, transitionDuration).OnComplete(() =>
            {
                screenToHide.gameObject.SetActive(false);
            });
        }

        currentScreen = screenToShow;
    }

    public void GoToNextScreen()
    {
        if (currentIndex < screenInfos.Count - 1)
        {
            ShowScreen(screenInfos[currentIndex + 1].screenName, true);
        }
    }
    public void GoToPreviousScreen()
    {
        if (currentIndex > 0)
        {
            ShowScreen(screenInfos[currentIndex - 1].screenName, false);
        }
    }

    public void OpenInitialScreen()
    {
        ShowScreen(screenInfos[0].screenName, true);
    }

    public void OpenLoginEmailScreen()
    {
        ShowScreen(screenInfos[11].screenName, true);
    }

    public void OpenProfileScreen()
    {
        ShowScreen(screenInfos[12].screenName, true);
    }

    public RectTransform GetCurrentScreen()
    {
        return currentScreen;
    }
}