using UnityEngine;

public interface IUIManager
{
    void ShowScreen(string screenName, bool forward);
    void GoToNextScreen();
    void GoToPreviousScreen();
    void OpenInitialScreen();
    void OpenLoginEmailScreen();
    void OpenProfileScreen();
    RectTransform GetCurrentScreen();
}