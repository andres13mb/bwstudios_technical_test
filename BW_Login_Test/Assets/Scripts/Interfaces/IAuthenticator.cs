public interface IAuthenticator
{
    // Métodos para autenticación
    void SignInWithEmail(string email, string password);
    void SignInWithGoogle();
    void Register(string email, string password, string name);
    void SignOut();
    string GetUserId();
    string GetUserName();
    string GetUserEmail();
    bool GetIsByEmail();
}