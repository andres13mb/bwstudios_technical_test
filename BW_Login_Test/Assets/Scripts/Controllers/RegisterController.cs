using UnityEngine;
using Zenject;
using TMPro;
using System;

public class RegisterController : MonoBehaviour
{
    // Campos p�blicos
    public TMP_InputField nameInputField;
    public TMP_InputField emailInputField;
    public TMP_InputField passwordInputField;
    public TextMeshProUGUI errorMessageText;
    private bool isFirstTime = true;

    // Dependencias
    [Inject] private IAuthenticator _authenticator;


    // M�todo de inicializaci�n
    public void Initialize(IAuthenticator authenticator)
    {
        _authenticator = authenticator;
    }

    private void Start()
    {
        if(isFirstTime)
        {
            InitData();
            isFirstTime = false;
        }
        
    }

    private void OnEnable()
    {
        if(!isFirstTime)
        {
            InitData();
        }
    }

    private void InitData()
    {
        if (PlayerPrefs.HasKey("name"))
        {
            nameInputField.text = PlayerPrefs.GetString("name");
        }

        emailInputField.text = string.Empty;
        passwordInputField.text = string.Empty;
        errorMessageText.text = string.Empty;
    }

    // Registrar nuevo usuario
    public void Register()
    {
        try
        {
            string email = emailInputField.text;
            string password = passwordInputField.text;
            string name = nameInputField.text;

            _authenticator.Register(email, password, name);

            PlayerPrefs.SetString("email", email);
            PlayerPrefs.SetString("pass", password);
            PlayerPrefs.SetString("name", nameInputField.text);
        }
        catch (Exception e)
        {
            Debug.LogError(e.ToString());
            errorMessageText.text = e.Message;
        }
    }
}

