using UnityEngine;
using UnityEngine.UI;
using System;
using Zenject;
using TMPro;

public class LoginController : MonoBehaviour
{
    // Campos p�blicos
    public TMP_InputField emailInputField;
    public TMP_InputField passwordInputField;
    public TextMeshProUGUI errorMessageText;
    private bool isFirstTime = true;

    // Inyecci�n de dependencias
    [Inject] private IAuthenticator _authenticator;

    // Constructor
    [Inject]
    public void Initialize(IAuthenticator authenticator)
    {
        _authenticator = authenticator;
    }

    private void Start()
    {
        if (isFirstTime)
        {
            InitialData();
            isFirstTime = false;
        }
    }

    private void OnEnable()
    {
        if (!isFirstTime)
        {
            InitialData();
        }
    }

    private void InitialData()
    {
        if (PlayerPrefs.HasKey("id"))
        {
            if(!PlayerPrefs.HasKey("pass"))
            {
                FirebaseGoogleLogin.Get().GoogleSignInClick();
            }
            else
            {
                emailInputField.text = PlayerPrefs.GetString("email");
                passwordInputField.text = PlayerPrefs.GetString("pass");

                SignInWithEmail();
            }            
        }
    }

    // Inicio de sesi�n con correo electr�nico
    public void SignInWithEmail()
    {
        try
        {
            PlayerPrefs.SetString("email", emailInputField.text); // solo por la demo 
            PlayerPrefs.SetString("pass", passwordInputField.text); // solo por la demo 

            _authenticator.SignInWithEmail(emailInputField.text, passwordInputField.text);
        }
        catch (Exception e)
        {
            errorMessageText.text = e.Message;
        }
    }

    // Inicio de sesi�n con Google
    public void SignInWithGoogle()
    {

    }

    public void ClearInputTexts()
    {
        emailInputField.text = string.Empty;
        passwordInputField.text= string.Empty;
        errorMessageText.text= string.Empty;
    }
}
