using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;
using Zenject;

public class ProfileController : MonoBehaviour
{
    public Button closeAppButton;
    public Button logoutButton;
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI emailText;
    public TextMeshProUGUI idText;
    public Image imageProfile;
    private string pass;
    private bool isFirstTime = true;

    [Inject] private IAuthenticator _authenticator;

    [Inject]
    public void Initialize(IAuthenticator authenticator)
    {
        _authenticator = authenticator;
    }

    void Start()
    {
        if (closeAppButton != null)
            closeAppButton.onClick.AddListener(CloseApp);

        if (logoutButton != null)
            logoutButton.onClick.AddListener(LogOut);

        if(isFirstTime)
        {
            StartData();
            isFirstTime = false;
        }
    }

    private void OnEnable()
    {
        if(!isFirstTime)
        {
            StartData();
        }
    }

    public void StartData()
    {
        if(PlayerPrefs.HasKey("pass"))
        {
            imageProfile.sprite = null;
        }

        if (!PlayerPrefs.HasKey("id"))
        {
            //nameText.text = _authenticator.GetUserName();

            nameText.text = PlayerPrefs.GetString("name");
            emailText.text = _authenticator.GetUserEmail();
            idText.text = _authenticator.GetUserId();
            SimpleSaveData();
        }
        else
        {
            SimpleLoadData();
        }
    }

    public void SimpleSaveData()
    {
        PlayerPrefs.SetString("name", nameText.text);
        PlayerPrefs.SetString("email", emailText.text);
        PlayerPrefs.SetString("id", idText.text);
    }

    public void SimpleLoadData()
    {
        nameText.text = PlayerPrefs.GetString("name");
        emailText.text = PlayerPrefs.GetString("email");
        idText.text = PlayerPrefs.GetString("id");
        pass = PlayerPrefs.GetString("pass");
    }

    public void DeleteAllData()
    {
        PlayerPrefs.DeleteAll();
    }

    private void CloseApp()
    {
        Debug.Log("Quit");
        Application.Quit();
    }

    private void LogOut()
    {
        if(_authenticator.GetIsByEmail())
        {
            _authenticator.SignOut();
        }
        else
        {
            FirebaseGoogleLogin.Get().LogOut();
        }
    }
}
