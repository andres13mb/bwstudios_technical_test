using Firebase;
using Firebase.Database;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataBaseService : MonoBehaviour
{
    DatabaseReference databaseReference;

    void Start()
    {
        // Configura la referencia de Firebase
        //FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("tu_url_de_firebase_aqu�");
        databaseReference = FirebaseDatabase.DefaultInstance.RootReference;

        // Escribe datos en Firebase
        WriteNewUser("123", "Juan", "juan@ejemplo.com");

        // Lee datos de Firebase
        ReadUserData("123");
    }

    void WriteNewUser(string userId, string name, string email)
    {
        User user = new User(name, email);
        string json = JsonUtility.ToJson(user);

        databaseReference.Child("users").Child(userId).SetRawJsonValueAsync(json);
    }

    void ReadUserData(string userId)
    {
        FirebaseDatabase.DefaultInstance.GetReference("users").Child(userId).GetValueAsync().ContinueWith(task => {
            if (task.IsFaulted)
            {
                // Manejar error...
            }
            else if (task.IsCompleted)
            {
                DataSnapshot snapshot = task.Result;
                // Hacer algo con los datos obtenidos, por ejemplo:
                Debug.Log(snapshot.GetRawJsonValue());
            }
        });
    }

    class User
    {
        public string username;
        public string email;

        public User(string username, string email)
        {
            this.username = username;
            this.email = email;
        }
    }
}
