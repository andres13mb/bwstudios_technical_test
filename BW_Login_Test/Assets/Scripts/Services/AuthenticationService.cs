using Firebase.Auth;
using Firebase.Extensions;
using System;
using System.Threading.Tasks;
using UnityEngine;
using Zenject;

public class AuthenticationService : IAuthenticator
{
    private FirebaseAuth auth;
    [Inject] private IUIManager uIManager;
    private bool isByEmail;

    public AuthenticationService()
    {
        auth = FirebaseAuth.DefaultInstance;
    }

    public void SignInWithEmail(string email, string password)
    {
        auth.SignInWithEmailAndPasswordAsync(email, password).ContinueWithOnMainThread(task =>
        {
            if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password))
            {
                if (uIManager.GetCurrentScreen().GetComponent<LoginController>())
                {
                    uIManager.GetCurrentScreen().GetComponent<LoginController>().errorMessageText.text = "Fill the inputfields.";
                }
                return;
            }

            if (task.IsCanceled)
            {
                if (uIManager.GetCurrentScreen().GetComponent<LoginController>())
                {
                    uIManager.GetCurrentScreen().GetComponent<LoginController>().errorMessageText.text = "Sign in was canceled.";
                }

                Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                throw new Exception("SignInWithEmailAndPasswordAsync was canceled.");

            }
            if (task.IsFaulted)
            {
                if(uIManager.GetCurrentScreen().GetComponent<LoginController>())
                {
                    uIManager.GetCurrentScreen().GetComponent<LoginController>().errorMessageText.text = "Sign in error.";
                }

                Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                throw new Exception("SignInWithEmailAndPasswordAsync encountered an error.");
            }

            FirebaseUser newUser = task.Result.User;
            Debug.LogFormat("User signed in successfully: {0} ({1})", newUser.DisplayName, newUser.UserId);

            if (uIManager.GetCurrentScreen().GetComponent<LoginController>())
            {
                uIManager.GetCurrentScreen().GetComponent<LoginController>().errorMessageText.text = "User signed in successfully.";
                uIManager.GetCurrentScreen().GetComponent<LoginController>().ClearInputTexts();

                isByEmail = true;
                uIManager.OpenProfileScreen();
            }
        });
    }

    public void SignInWithGoogle()
    {

    }

    // Registrar un usuario con correo y contrase�a
    public void Register(string email, string password, string name)
    {
        auth.CreateUserWithEmailAndPasswordAsync(email, password).ContinueWithOnMainThread(task =>
        {
            // Verificar que los campos de correo y contrase�a no est�n vac�os
            if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password))
            {
                if (uIManager.GetCurrentScreen().GetComponent<RegisterController>())
                {
                    uIManager.GetCurrentScreen().GetComponent<RegisterController>().errorMessageText.text = "Fill the inputfields.";
                }

                return;
            }

            if (task.IsCanceled)
            {
                // Mensaje de error en caso de que la operaci�n de registro se haya cancelado
                if (uIManager.GetCurrentScreen().GetComponent<RegisterController>())
                {
                    uIManager.GetCurrentScreen().GetComponent<RegisterController>().errorMessageText.text = "Create User Error.";
                }
                Debug.LogError("CreateUserWithEmailAndPasswordAsync was canceled.");
                throw new Exception("CreateUserWithEmailAndPasswordAsync was canceled.");
            }
            if (task.IsFaulted)
            {
                // Mensaje de error en caso de que se haya producido un error durante la operaci�n de registro
                if (uIManager.GetCurrentScreen().GetComponent<RegisterController>())
                {
                    uIManager.GetCurrentScreen().GetComponent<RegisterController>().errorMessageText.text = "Create User Error.";
                }

                Debug.LogError("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                throw new Exception("CreateUserWithEmailAndPasswordAsync encountered an error.");
            }

            FirebaseUser newUser = task.Result.User;
            // Mensaje de confirmaci�n y redirecci�n a la pantalla de inicio de sesi�n
            Debug.LogFormat("User created successfully: {0} ({1})", newUser.DisplayName, newUser.UserId);

            if (uIManager.GetCurrentScreen().GetComponent<RegisterController>())
            {
                uIManager.GetCurrentScreen().GetComponent<RegisterController>().errorMessageText.text = "User created successfully.";

                isByEmail = true;
                uIManager.OpenProfileScreen();
            }        
        });
    }

    // Cerrar sesi�n
    public void SignOut()
    {
        isByEmail = false;

        auth.SignOut();

        if (uIManager.GetCurrentScreen().GetComponent<ProfileController>())
        {
            uIManager.GetCurrentScreen().GetComponent<ProfileController>().DeleteAllData();
            uIManager.OpenInitialScreen();

            Debug.Log("Log out via email");
        }

    }

    public string GetUserId()
    {
        FirebaseUser user = auth.CurrentUser;
        return user.UserId;
    }
    public string GetUserName()
    {
        FirebaseUser user = auth.CurrentUser;
        return user.DisplayName;
    }
    public string GetUserEmail()
    {
        FirebaseUser user = auth.CurrentUser;
        return user.Email;
    }
    public bool GetIsByEmail()
    {
        return isByEmail;
    }
}