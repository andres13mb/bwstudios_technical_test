using Firebase;
using Firebase.Database;
using Firebase.Extensions;
using UnityEngine;

public class UserService
{
    private DatabaseReference reference;

    public UserService()
    {
        // Verifica y corrige las dependencias de Firebase en el hilo principal.
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWithOnMainThread(task =>
        {
            if (task.Exception != null)
            {
                // Si hay un error, muestra un mensaje de error en la consola de depuración.
                Debug.LogError($"Error al inicializar Firebase: {task.Exception}");
                return;
            }

            // Si todo va bien, establece la referencia a la base de datos en la instancia predeterminada de FirebaseDatabase.
            reference = FirebaseDatabase.DefaultInstance.RootReference;
        });
    }

    public void SubscribeToUserData(string userId, IDataDisplay dataDisplay)
    {
        // Obtiene la referencia al nodo del usuario en la base de datos y suscribe a los cambios en el valor.
        FirebaseDatabase.DefaultInstance
            .GetReference($"users/{userId}")
            .ValueChanged += (object sender, ValueChangedEventArgs args) =>
            {
                if (args.DatabaseError != null)
                {
                    // Si hay un error en la base de datos, muestra un mensaje de error en la consola de depuración.
                    Debug.LogError(args.DatabaseError.Message);
                    return;
                }

                // Si no hay errores, obtiene los datos del usuario como un objeto UserModel y los muestra en la interfaz de usuario.
                UserModel user = JsonUtility.FromJson<UserModel>(args.Snapshot.GetRawJsonValue());
                dataDisplay.DisplayUserData(user);
            };
    }
}